use crate::glib_utils::{RustedListBox, RustedListStore};
use crate::invidious::core::{SearchParams, SortBy, TrendingVideo};
use crate::widgets::VideoRow;
use crate::{ctx, Client};

use ev_stream_gtk_rs::ev_stream;
use futures::future::RemoteHandle;
use futures::join;
use futures::prelude::*;
use futures::task::LocalSpawnExt;
use glib::clone;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use libadwaita as adw;
use once_cell::unsync::OnceCell;

mod imp {
    use super::*;

    use gtk::CompositeTemplate;

    #[derive(CompositeTemplate)]
    #[template(resource = "/com/ranfdev/SharMaVid/ui/search_page.ui")]
    pub struct SearchPage {
        #[template_child]
        pub video_list: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub search_entry: TemplateChild<gtk::SearchEntry>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub no_results_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub scrolled_window: TemplateChild<gtk::ScrolledWindow>,
        #[template_child]
        pub spinner_page: TemplateChild<gtk::Box>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
        pub video_list_model: RustedListStore<TrendingVideo>,
        pub async_handle: OnceCell<Option<RemoteHandle<()>>>,
    }

    impl Default for SearchPage {
        fn default() -> Self {
            Self {
                video_list: TemplateChild::default(),
                video_list_model: RustedListStore::new(),
                search_entry: TemplateChild::default(),
                stack: TemplateChild::default(),
                spinner: TemplateChild::default(),
                no_results_page: TemplateChild::default(),
                spinner_page: TemplateChild::default(),
                scrolled_window: TemplateChild::default(),
                async_handle: OnceCell::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchPage {
        const NAME: &'static str = "SearchPage";
        type Type = super::SearchPage;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl WidgetImpl for SearchPage {}
    impl ObjectImpl for SearchPage {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.setup_widgets();
            obj.setup_signals();
        }
    }
    impl BoxImpl for SearchPage {}
}

glib::wrapper! {
    pub struct SearchPage(ObjectSubclass<imp::SearchPage>)
        @extends gtk::Widget, gtk::Box;
}

impl SearchPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create SearchPage")
    }
    fn setup_widgets(&self) {
        let imp = self.imp();

        imp.video_list
            .bind_rusted_model(&imp.video_list_model, |v| VideoRow::new(v.clone()).upcast());

        let search_entry = imp.search_entry.clone();
        glib::source::idle_add_local(move || {
            search_entry.grab_focus();
            Continue(false)
        });
    }
    fn setup_signals(&self) {
        let imp = self.imp();

        let search_entry = imp.search_entry.clone();
        let search_queries_fut = ev_stream!(imp.search_entry, search_changed, |entry|)
            .map(move |_| search_entry.text().to_string())
            .map(|text| {
                let mut params = SearchParams::default();
                params.query = text;
                params.sort_by = Some(SortBy::Relevance);
                params
            })
            .fold(
                None::<RemoteHandle<()>>,
                clone!(@weak self as this => @default-return future::ready(None), move |_, params| {
                    future::ready(this.handle_search_changed(params))
                }),
            );

        let row_clicks_fut = ev_stream!(imp.video_list, row_activated, |list, row|)
            .filter_map(|(_, row)| future::ready(row.downcast().ok()))
            .for_each(|row: VideoRow| {
                future::ready(
                    row.activate_action("win.view-video", Some(&row.video().video_id.to_variant()))
                        .unwrap(),
                )
            });

        let handle = ctx()
            .spawn_local_with_handle(async move {
                join!(search_queries_fut, row_clicks_fut,);
            })
            .ok();
        imp.async_handle.set(handle).unwrap();
    }
    fn handle_search_changed(&self, params: SearchParams) -> Option<RemoteHandle<()>> {
        let imp = self.imp();

        imp.spinner.start();
        imp.stack.set_visible_child(&*imp.spinner_page);
        imp.video_list_model.clear();

        let search_results = Client::global().search(params);
        let bottom_hits = ev_stream!(imp.scrolled_window, edge_reached, |win, edge|)
            .filter(|(_, edge)| future::ready(*edge == gtk::PositionType::Bottom))
            .map(|_| ());

        let fut = stream::once(async { () }) // Do one initial fetch
            .chain(bottom_hits)
            .zip(search_results)
            .filter_map(|(_, res)| future::ready(res.ok()))
            .enumerate()
            .for_each(
                clone!(@weak self as this => @default-return future::ready(()), move |(i, res)| {
                    let imp = this.imp();
                    let no_results = i == 0 && res.len() == 0;
                    imp.video_list_model.extend(res.into_iter());
                    log::info!("Results loaded");

                    if no_results {
                        imp.stack.set_visible_child(&*imp.no_results_page);
                    } else {
                        imp.stack.set_visible_child(&*imp.scrolled_window);
                    }
                    imp.spinner.stop();
                    future::ready(())
                }),
            );

        ctx().spawn_local_with_handle(fut).ok()
    }
}
